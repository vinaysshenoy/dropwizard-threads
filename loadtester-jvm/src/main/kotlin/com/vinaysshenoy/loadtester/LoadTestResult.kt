package com.vinaysshenoy.loadtester

data class LoadTestResult(

		val avgRequestDuration: Long = 0L,

		val requestsPerSecond: Long = 0L,

		val numFailures: Long = 0L,

		val numErrors: Long = 0L
)