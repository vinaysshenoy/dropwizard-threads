package com.vinaysshenoy.loadtester

import okhttp3.*
import java.io.IOException
import java.time.Duration
import java.time.Instant
import java.util.Timer
import java.util.TimerTask
import java.util.concurrent.TimeUnit

fun main(args: Array<String>) {
	LoadTest().run(*args)
}

class LoadTest {

	private val okHttp: OkHttpClient
	private val measuringInterceptor = MeasuringInterceptor()
	private val timer = Timer()

	init {
		val dispatcher = Dispatcher()
		dispatcher.maxRequests = Int.MAX_VALUE
		dispatcher.maxRequestsPerHost = Int.MAX_VALUE

		okHttp = OkHttpClient.Builder()
				.addInterceptor(measuringInterceptor)
				.connectTimeout(0L, TimeUnit.SECONDS)
				.readTimeout(0L, TimeUnit.SECONDS)
				.dispatcher(dispatcher)
				.build()
	}

	fun run(vararg args: String) {
		val arguments = args.toList()
				.windowed(size = 2, step = 2)
				.map { it[0] to it[1] }
				.toMap()

		val testDuration = Duration.ofSeconds(arguments["duration"]?.toLong() ?: 60L)
		val till = Instant.now()
				.plusMillis(testDuration.toMillis())

		println("Running load test for $testDuration")

		timer.scheduleAtFixedRate(
				CheckFinished(timer, okHttp, measuringInterceptor, testDuration),
				testDuration.toMillis(),
				Duration.ofSeconds(5L).toMillis()
		)
		timer.scheduleAtFixedRate(QueueRequest(okHttp, till), 0L, Duration.ofMillis(1L).toMillis())
	}

	private class Finish(
			private val timer: Timer,
			private val okHttpClient: OkHttpClient
	) : TimerTask() {

		override fun run() {
			timer.cancel()
			okHttpClient.dispatcher()
					.executorService()
					.shutdown()
		}
	}

	private class CheckFinished(
			private val timer: Timer,
			private val okHttpClient: OkHttpClient,
			private val measuringInterceptor: MeasuringInterceptor,
			private val testDuration: Duration
	) : TimerTask() {

		private data class ResultCount(
				var successful: Long = 0L,
				var failed: Long = 0L,
				var errors: Long = 0L,
				var total: Long = 0L
		)

		override fun run() {
			val dispatcher = okHttpClient.dispatcher()

			//All requests are done, save results and finish
			if (dispatcher.queuedCallsCount() + dispatcher.runningCallsCount() == 0) {
				val resultCount = measuringInterceptor.results
						.fold(ResultCount(), { acc, requestResult ->
							when (requestResult) {
								is Success -> acc.successful++
								is RequestFailed -> acc.failed++
								is Error -> acc.errors++
							}
							acc.total++
							acc
						})

				val successfulRequests = measuringInterceptor.results
						.filter { it is Success }
						.map { it as Success }

				var loadTestResult =
						LoadTestResult(numFailures = resultCount.failed, numErrors = resultCount.errors)
				loadTestResult = loadTestResult.copy(
						avgRequestDuration = successfulRequests.map { it.duration }
								.reduce { acc, duration -> acc + duration }
								.div(successfulRequests.size),
						requestsPerSecond = successfulRequests.size / testDuration.seconds
				)

				measuringInterceptor.results
						.filter { it is Error }
						.map { it as Error }
						.forEach { it.cause.printStackTrace() }

				println("Results: $loadTestResult")

				timer.schedule(Finish(timer, okHttpClient), 0L)
			}
		}
	}

	private class QueueRequest(
			private val okHttpClient: OkHttpClient,
			till: Instant
	) : TimerTask() {

		private val endTime = till.toEpochMilli()

		override fun run() {

			if (endTime < System.currentTimeMillis()) {
				return
			}

			val request = Request.Builder()
					.url("http://localhost:8080/syncResource")
					.build()

			okHttpClient.newCall(request)
					.enqueue(object : Callback {
						override fun onFailure(
								call: Call,
								e: IOException
						) {
							//Handled in interceptor
						}

						override fun onResponse(
								call: Call,
								response: Response
						) {
							//Handled in interceptor
						}
					})
		}
	}
}