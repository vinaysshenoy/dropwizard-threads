package com.vinaysshenoy.loadtester

import java.time.Instant

sealed class RequestResult

data class Success(
		val startTime: Instant,
		val endTime: Instant,
		val statusCode: Int
) : RequestResult() {

	val duration = endTime.minusMillis(startTime.toEpochMilli())
			.toEpochMilli()
}

data class RequestFailed(
		val startTime: Instant,
		val endTime: Instant,
		val statusCode: Int
) : RequestResult() {
	val duration = endTime.minusMillis(startTime.toEpochMilli())
			.toEpochMilli()
}

data class Error(val cause: Throwable): RequestResult()