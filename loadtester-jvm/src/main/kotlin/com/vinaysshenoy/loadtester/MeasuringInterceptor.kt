package com.vinaysshenoy.loadtester

import okhttp3.Interceptor
import okhttp3.Response
import java.time.Instant

class MeasuringInterceptor : Interceptor {

	private val resultStore = mutableListOf<RequestResult>()

	val results
		get() = resultStore.toList()

	override fun intercept(chain: Interceptor.Chain): Response {

		val start = Instant.now()
		try {
			val response = chain.proceed(chain.request())
			//We don't actually use the response anywhere, so just consume it
			response.body()
					?.bytes()
			val end = Instant.now()

			pushResult(
					when {
						response.isSuccessful -> Success(start, end, response.code())
						else -> RequestFailed(start, end, response.code())
					}
			)
			return response
		} catch (e: Exception) {
			pushResult(Error(e))
			throw e
		}
	}

	private fun pushResult(requestResult: RequestResult) {
		synchronized(resultStore) {
			resultStore.add(requestResult)
		}
	}
}