# OpenJdk 8, Debian Stretch
FROM openjdk:8-jre-stretch

WORKDIR /app

ADD build/libs/bundle.jar /app/bundle.jar
# ADD appconfig.yml /app/appconfig.yml
ADD data_items.json /app/data_items.json
ADD data_users.json /app/data_users.json

EXPOSE 8080

# to print JVM flags, add '-XX:+PrintFlagsFinal' after 'java'

CMD java $JAVA_OPTIONS -jar bundle.jar server appconfig.yml