package com.vinaysshenoy.dropwizardthreads.rxresource

import com.vinaysshenoy.dropwizardthreads.AppConfig
import com.vinaysshenoy.dropwizardthreads.common.mapItemModelToItem
import com.vinaysshenoy.dropwizardthreads.common.mapUserModelToUser
import com.vinaysshenoy.dropwizardthreads.common.mapUserToUserResponseModel
import com.vinaysshenoy.dropwizardthreads.data.ApiResponseModel
import com.vinaysshenoy.dropwizardthreads.data.ItemModel
import com.vinaysshenoy.dropwizardthreads.data.UserModel
import com.vinaysshenoy.dropwizardthreads.domain.User
import io.dropwizard.setup.Environment
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.Singles.zip
import io.reactivex.schedulers.Schedulers
import org.slf4j.LoggerFactory
import java.util.concurrent.Executors
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.container.AsyncResponse
import javax.ws.rs.container.Suspended
import javax.ws.rs.core.MediaType

@Path("rxResource")
@Produces(MediaType.APPLICATION_JSON)
class RxResource(
		environment: Environment,
		config: AppConfig
) {

	private val objectMapper = environment.objectMapper

	private val logger = LoggerFactory.getLogger("RxResource")

	private val ioPool = when {
		config.ioThreads <= 0 -> Schedulers.io()
		else -> Schedulers.from(Executors.newFixedThreadPool(config.ioThreads))
	}

	private val computationPool = Schedulers.computation()

	@GET
	fun get(@Suspended resp: AsyncResponse) {
		zip(readUsersFromFile(), readItemsFromFile())
				.observeOn(computationPool)
				.flatMap { (userModels, itemModels) -> matchItemsToUsers(userModels, itemModels) }
				.flatMap(this::convertToDomain)
				.flatMap(this::convertToResponse)
				.subscribe({ resp.resume(it) }, { resp.resume(it) })
	}

	private fun readUsersFromFile() = Single.fromCallable {
		com.vinaysshenoy.dropwizardthreads.common.readUsersFromFile(objectMapper)
	}.subscribeOn(ioPool)!!

	private fun readItemsFromFile() =
			Single.fromCallable {
				com.vinaysshenoy.dropwizardthreads.common.readItemsFromFile(objectMapper)
			}.subscribeOn(ioPool)!!

	private fun matchItemsToUsers(
			users: List<UserModel>,
			items: List<ItemModel>
	): Single<Map<UserModel, List<ItemModel>>> {

		return Single.fromCallable {
			val usersById = users.map { it.id to it }
					.toMap()

			items
					.groupBy { it.userId }
					.map { (userId, items) -> usersById[userId]!! to items }
					.toMap()
		}!!
	}

	private fun convertToDomain(data: Map<UserModel, List<ItemModel>>): Single<List<User>> {
		return Single.fromCallable {
			data.entries
					.map { (userModel, itemModels) ->
						mapUserModelToUser(userModel) to itemModels.map { mapItemModelToItem(it) }
					}
					.map { (user, items) -> user.copy(items = items) }
		}!!
	}

	private fun convertToResponse(users: List<User>): Single<ApiResponseModel> {
		return Single.fromCallable {
			users
					.map { mapUserToUserResponseModel(it) }
					.let { ApiResponseModel(data = it) }
		}!!
	}
}