package com.vinaysshenoy.dropwizardthreads.syncresource

import com.vinaysshenoy.dropwizardthreads.common.*
import com.vinaysshenoy.dropwizardthreads.data.ApiResponseModel
import com.vinaysshenoy.dropwizardthreads.data.ItemModel
import com.vinaysshenoy.dropwizardthreads.data.UserModel
import com.vinaysshenoy.dropwizardthreads.domain.User
import io.dropwizard.setup.Environment
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("syncResource")
@Produces(MediaType.APPLICATION_JSON)
class SyncResource(environment: Environment) {

	private val objectMapper = environment.objectMapper

	@GET
	fun get(): Response {

		try {
			val userModels = readUsersFromFile(objectMapper)
			val itemModels = readItemsFromFile(objectMapper)

			val users = matchItemsToUsers(userModels, itemModels).let { convertToDomain(it) }

			val responseEntity = convertToResponse(users)
			return Response.ok(responseEntity)
					.build()
		} catch (e: Exception) {
			e.printStackTrace()
			throw e
		}
	}

	private fun matchItemsToUsers(
			users: List<UserModel>,
			items: List<ItemModel>
	): Map<UserModel, List<ItemModel>> {

		val usersById = users.map { it.id to it }
				.toMap()

		return items
				.groupBy { it.userId }
				.map { (userId, items) -> usersById[userId]!! to items }
				.toMap()
	}

	private fun convertToDomain(data: Map<UserModel, List<ItemModel>>) =
			data.entries
					.map { (userModel, itemModels) ->
						mapUserModelToUser(userModel) to itemModels.map { mapItemModelToItem(it) }
					}
					.map { (user, items) -> user.copy(items = items) }

	private fun convertToResponse(users: List<User>): ApiResponseModel {
		return users
				.map { mapUserToUserResponseModel(it) }
				.let { ApiResponseModel(data = it) }
	}

}
