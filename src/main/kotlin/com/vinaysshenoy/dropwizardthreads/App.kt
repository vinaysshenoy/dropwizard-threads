package com.vinaysshenoy.dropwizardthreads

import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.vinaysshenoy.dropwizardthreads.rxresource.RxResource
import com.vinaysshenoy.dropwizardthreads.syncresource.SyncResource
import io.dropwizard.Application
import io.dropwizard.server.DefaultServerFactory
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment

fun main(args: Array<String>) {
	App().run(*args)
}

class App : Application<AppConfig>() {

	override fun run(
			configuration: AppConfig,
			environment: Environment
	) {
		println(
				"Started Application: ${configuration.appName} with ${(configuration.serverFactory as DefaultServerFactory).maxThreads} threads, ${configuration.ioThreads} IO Threads"
		)

		environment.jersey()
				.apply {
					register(SyncResource(environment))
					register(RxResource(environment, configuration))
				}
	}

	override fun initialize(bootstrap: Bootstrap<AppConfig>) {
		bootstrap.objectMapper.registerModule(KotlinModule())
	}
}