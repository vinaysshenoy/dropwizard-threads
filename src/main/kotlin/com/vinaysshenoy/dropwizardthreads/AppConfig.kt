package com.vinaysshenoy.dropwizardthreads

import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.Configuration

class AppConfig : Configuration() {

	@JsonProperty("appName")
	lateinit var appName: String

	@JsonProperty("ioThreads")
	var ioThreads: Int = 0
}