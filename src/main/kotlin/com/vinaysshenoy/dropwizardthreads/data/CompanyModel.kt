package com.vinaysshenoy.dropwizardthreads.data

import com.fasterxml.jackson.annotation.JsonProperty

data class CompanyModel(

		@JsonProperty("name")
		val name: String,

		@JsonProperty("catchPhrase")
		val catchPhrase: String,

		@JsonProperty("bs")
		val bs: String
)