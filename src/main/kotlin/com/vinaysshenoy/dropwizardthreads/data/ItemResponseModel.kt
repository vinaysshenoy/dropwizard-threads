package com.vinaysshenoy.dropwizardthreads.data

import com.fasterxml.jackson.annotation.JsonProperty

data class ItemResponseModel(

		@JsonProperty("id")
		val id: Int,

		@JsonProperty("title")
		val title: String,

		@JsonProperty("body")
		val body: String
)