package com.vinaysshenoy.dropwizardthreads.data

import com.fasterxml.jackson.annotation.JsonProperty

data class ItemModel(

		@JsonProperty("body")
		val body: String,

		@JsonProperty("id")
		val id: Int,

		@JsonProperty("title")
		val title: String,

		@JsonProperty("userId")
		val userId: Int
)