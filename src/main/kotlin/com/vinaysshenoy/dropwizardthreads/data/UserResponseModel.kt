package com.vinaysshenoy.dropwizardthreads.data

import com.fasterxml.jackson.annotation.JsonProperty

data class UserResponseModel(

		@JsonProperty("user")
		val user: UserModel,

		@JsonProperty("items")
		val items: List<ItemResponseModel>
)