package com.vinaysshenoy.dropwizardthreads.data

import com.fasterxml.jackson.annotation.JsonProperty

data class GeoLocationModel(

		@JsonProperty("lat")
		val lat: Double,

		@JsonProperty("lng")
		val long: Double
)