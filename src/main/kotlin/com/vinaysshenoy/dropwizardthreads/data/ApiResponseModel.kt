package com.vinaysshenoy.dropwizardthreads.data

import com.fasterxml.jackson.annotation.JsonProperty

data class ApiResponseModel(

		@JsonProperty("data")
		val data: List<UserResponseModel>
)