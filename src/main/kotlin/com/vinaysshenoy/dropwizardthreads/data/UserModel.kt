package com.vinaysshenoy.dropwizardthreads.data

import com.fasterxml.jackson.annotation.JsonProperty

data class UserModel(

		@JsonProperty("id")
		val id: Int,

		@JsonProperty("name")
		val name: String,

		@JsonProperty("username")
		val username: String,

		@JsonProperty("email")
		val email: String,

		@JsonProperty("address")
		val address: AddressModel,

		@JsonProperty("phone")
		val phone: String,

		@JsonProperty("website")
		val website: String,

		@JsonProperty("company")
		val company: CompanyModel
)