package com.vinaysshenoy.dropwizardthreads.data

import com.fasterxml.jackson.annotation.JsonProperty

data class AddressModel(

		@JsonProperty("street")
		val street: String,

		@JsonProperty("suite")
		val suite: String,

		@JsonProperty("city")
		val city: String,

		@JsonProperty("zipcode")
		val zipCode: String,

		@JsonProperty("geo")
		val geo: GeoLocationModel
)