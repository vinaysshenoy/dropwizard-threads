package com.vinaysshenoy.dropwizardthreads.common

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.vinaysshenoy.dropwizardthreads.data.*
import com.vinaysshenoy.dropwizardthreads.domain.*
import java.io.BufferedReader
import java.io.File

fun readUsersFromFile(objectMapper: ObjectMapper) =
		File("data_users.json").reader()
				.let { BufferedReader(it, 512) }
				.use { objectMapper.readValue(it) as List<UserModel> }

fun readItemsFromFile(objectMapper: ObjectMapper) =
		File("data_items.json").reader()
				.let { BufferedReader(it, 512) }
				.use { objectMapper.readValue(it) as List<ItemModel> }

fun mapUserModelToUser(userModel: UserModel) = User(
		id = userModel.id,
		name = userModel.name,
		username = userModel.username,
		email = userModel.email,
		address = mapAddressModelToAddress(userModel.address),
		company = mapCompanyModelToCompany(userModel.company),
		phone = userModel.phone,
		website = userModel.website,
		items = emptyList()
)

private fun mapCompanyModelToCompany(companyModel: CompanyModel) = Company(
		name = companyModel.name,
		catchPhrase = companyModel.catchPhrase,
		bs = companyModel.bs
)

private fun mapAddressModelToAddress(addressModel: AddressModel) = Address(
		street = addressModel.street,
		suite = addressModel.suite,
		city = addressModel.city,
		zipCode = addressModel.zipCode,
		geo = Geo(lat = addressModel.geo.lat, long = addressModel.geo.long)
)

fun mapItemModelToItem(itemModel: ItemModel) = Item(
		id = itemModel.id,
		title = itemModel.title,
		body = itemModel.body
)

fun mapUserToUserResponseModel(
		user: User
): UserResponseModel {
	return UserResponseModel(
			user = mapUserToUserModel(user),
			items = user.items.map { mapItemToItemResponseModel(it) }
	)
}

private fun mapItemToItemResponseModel(item: Item): ItemResponseModel {
	return ItemResponseModel(
			id = item.id,
			body = item.body,
			title = item.title
	)
}

private fun mapUserToUserModel(user: User): UserModel {
	return UserModel(
			id = user.id,
			name = user.name,
			username = user.username,
			email = user.email,
			address = mapAddressToAddressModel(user.address),
			phone = user.phone,
			website = user.website,
			company = mapCompanyToCompanyModel(user.company)
	)
}

private fun mapCompanyToCompanyModel(company: Company): CompanyModel {
	return CompanyModel(
			name = company.name,
			catchPhrase = company.catchPhrase,
			bs = company.bs
	)
}

private fun mapAddressToAddressModel(address: Address): AddressModel {
	return AddressModel(
			street = address.street,
			suite = address.suite,
			city = address.city,
			zipCode = address.zipCode,
			geo = GeoLocationModel(lat = address.geo.lat, long = address.geo.long)
	)
}