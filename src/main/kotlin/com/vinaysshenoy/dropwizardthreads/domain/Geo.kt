package com.vinaysshenoy.dropwizardthreads.domain

data class Geo(val lat: Double, val long: Double)