package com.vinaysshenoy.dropwizardthreads.domain

data class Item(
		val id: Int,
		val title: String,
		val body: String
)