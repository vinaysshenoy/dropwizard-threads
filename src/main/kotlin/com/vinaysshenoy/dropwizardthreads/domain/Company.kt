package com.vinaysshenoy.dropwizardthreads.domain

data class Company(
		val name: String,
		val catchPhrase: String,
		val bs: String
)