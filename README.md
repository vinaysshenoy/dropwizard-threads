# Dropwizard Threads
###### A project to test threading paradigms and their impact on performance on a dropwizard server

## TODO: //Add detailed documentation

## Useful commands
```bash
./gradlew assemble
docker build -t dwthreads .
docker run -d -e JAVA_OPTIONS '-Xmx100m' -p 8080:8080 dwthreads
docker logs -f <container id>

docker tag dwthreads:latest <dockerhubusername>/dwthreads:<version-number>
docker push <dockerhubusername>/dwthreads:<version-number>

docker-machine create -d virtualbox --virtualbox-cpu-count 1 --virtualbox-memory 1024 myvm

eval $(docker-machine env myvm)

docker swarm init

docker stack deploy -c docker-compose.yml dwthreads
docker stack rm dwthreads
docker service logs -f dwthreads_web | grep -i "metrics"

ab -c 300 -e results.csv -s 3600 -t 60 -n 10000000  -r http://localhost:8080/syncResource/

docker run --rm -m 256m -c=25000 jordi/ab -c 100 -s 3600 -t 60 -n 10000000 -r http://172.17.0.1:8080/syncResource/

docker run --rm -m 256m -c=25000 jordi/ab -c 100 -s 3600 -t 60 -n 10000000 -r -v 2 http://172.17.0.1:8080/syncResource/ | grep -i "apr_socket_recv"
docker run --rm -m 256m -c=25000 jordi/ab -c 400 -s 3600 -t 120 -n 10000000 -r  http://172.17.0.1:8080/syncResource/
```