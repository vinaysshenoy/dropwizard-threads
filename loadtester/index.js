const axios = require('axios')
const fs = require('fs')

const LOADTEST_DURATION_MINUTES = 1;

const client = axios.create({
  baseURL: 'http://localhost:8080/syncResource',
  timeout: 0,
  //Intentionally don't parse response so that it does not contribute the benchmarks
  transformResponse: data => data
})

const till = Date.now() + (LOADTEST_DURATION_MINUTES * 60 * 1000)

const results = []
const failures = []

const intervalClearToken = setInterval(sendRequest, 1)
var requestNumber = 1

var ongoingRequests = 0

function sendRequest() {
  const time = Date.now()

  if(time > till) {
    clearInterval(intervalClearToken)
    setTimeout(() => {
      console.log(`Wriing ${results.length} results to file`)
      fs.writeFileSync('results.json', JSON.stringify(results), 'utf8');
      console.log("Wrote results to file")

      console.log(`Wriing ${failures.length} failures to file`)
      fs.writeFileSync('failures.json', JSON.stringify(failures), 'utf8');
      console.log("Wrote failures to file")
    }, 11000)
    return
  }

  ongoingRequests++
  client.get('', {
      requestNumber,
      time
    })
    .then(response => {
      const now = Date.now()
      results.push({
        requestNumber: response.config.requestNumber,
        duration: now - response.config.time,
        startTime: response.config.time,
        endTime: now,
        status: response.status
      })
      console.log(`Finished request #${response.config.requestNumber}`)
      return response
    })
    .catch(err => {

      failures.push(err.toString())
    })
    .finally(() => { 
      ongoingRequests-- 
    })
  requestNumber++
}